package com.plugins;

import ij.CompositeImage;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.ImageWindow;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.macro.Interpreter;
import ij.measure.Calibration;
import ij.process.ImageProcessor;
import ij.process.LUT;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Copied from Original ij.plugin.StackEditor and only
 * using {@link #convertStackToImages(ImagePlus)} slightly modified
 * to return the stacks instead of displaying them.
 */
public final class StackEditor {

        public StackEditor() {
                
        }
        
        public final ImagePlus[] convertStackToImages(ImagePlus imp) {
                if(imp.getNSlices() < 2) {
                        return null;
                }
                
                if(!imp.lock()) {
                        return null;
                }
                
                ImageStack stack = imp.getStack();
                int size = stack.getSize();
                if(size > 30 && !IJ.isMacro()) {
                        boolean ok = IJ.showMessageWithCancel("Convert to Images?", "Are you sure you want to convert this\nstack to " + size + " separate windows?");
                        if(!ok) {
                                imp.unlock();
                                return null;
                        }
                }
                
                Calibration cal = imp.getCalibration();
                CompositeImage cimg = null;
                if(imp.isComposite()) {
                        cimg = (CompositeImage)imp;
                }
                
                if(imp.getNChannels() != imp.getStackSize()) {
                        cimg = null;
                }
                
                Overlay overlay = imp.getOverlay();
                ImagePlus[] imgStacks = new ImagePlus[size];
                
                for(int i = 1; i <= size; i++) {
                        String label = stack.getShortSliceLabel(i);
                        String title = label != null && !label.equals("") ? label : getTitle(imp, i);
                        ImageProcessor ip = stack.getProcessor(i);
                        
                        if(cimg != null) {
                                LUT lut = cimg.getChannelLut(i);
                                if(lut != null) {
                                        ip.setColorModel(lut);
                                        ip.setMinAndMax(lut.min, lut.max);
                                }
                        }
                        
                        ImagePlus imp2 = new ImagePlus(title, ip);
                        imp2.setCalibration(cal);
                        String info = stack.getSliceLabel(i);
                        if(info != null && !info.equals(label)) {
                                imp2.setProperty("Info", info);
                        }
                        
                        imp2.setIJMenuBar(i == size);
                        if(overlay != null) {
                                Overlay overlay2 = new Overlay();
                                for(int j = 0; j < overlay.size(); j++) {
                                        Roi roi = overlay.get(j);
                                        if(roi.getPosition() == i) {
                                                roi.setPosition(0);
                                                overlay2.add((Roi)roi.clone());
                                        }
                                }
                                
                                if(overlay2.size() > 0) {
                                        imp2.setOverlay(overlay2);
                                }
                        }
                        
                        imgStacks[i-1] = imp2;
                }
                
                imp.changes = false;
                ImageWindow win = imp.getWindow();
                if(win != null) {
                        win.close();
                }
                else if(Interpreter.isBatchMode()) {
                        Interpreter.removeBatchModeImage(imp);
                }
                
                imp.unlock();
                
                return imgStacks;
        }

        private final String getTitle(ImagePlus imp, int n) {
                String digits = "00000000" + n;
                return imp.getShortTitle() + "-" + digits.substring(digits.length() - 4, digits.length());
        }
}
