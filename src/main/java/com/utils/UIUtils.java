package com.utils;

import java.io.File;
import java.text.SimpleDateFormat;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import com.CerealTables;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * 
 * @author Alexi Akl (alexiakl@gmail.com)
 * 
 */
@SuppressWarnings("restriction")
public class UIUtils {
	public static TextArea logArea;
	public static TextField sheetId;
	public static TextField sheetName;
	public static TextField fromRow;
	public static TextField toRow;
	public static TextField focalLength;
	public static TextField hDistance;
	public static ImageView imageView;
	public static Text imageTitle;
	public static CheckBox earlyVigorAlgo;
	public static CheckBox earCountingAlgo;
	public static CheckBox maturityAlgo;
	public static Button buttonStart;
	public static Button buttonSave;
	public static Button buttonSettings;
	public static Button buttonStop;
	public static CheckBox processAllCheckbox;
	public static CheckBox processContinuouslyCheckbox;
	public static CheckBox processUnprocessedCheckbox;
	public static TextField columnNameFilterKey;
	public static TextField columnNameFilterValue;
	public static DatePicker fromDatePicker;
	public static DatePicker toDatePicker;
	public static GridPane wecGridPane;
	public static Stage settingsStage;
	static VBox filtervbox;
	private static StringConverter<LocalDate> dateConverter;
	private static HBox algoBox;
	private static VBox configBox;

	public static Scene buildUI() {
		initDateConverter();

		createSettingsStage();

		BorderPane root = new BorderPane();
		VBox topBox = addTopBox();
		root.setTop(topBox);
		VBox leftBox = addLeftBox();
		root.setLeft(leftBox);
		VBox imageBox = addImageBox();
		root.setCenter(imageBox);

		return new Scene(root, 820, 600);
	}

	private static void initDateConverter() {
		dateConverter = new StringConverter<LocalDate>() {
			private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

			@Override
			public String toString(LocalDate localDate) {
				if (localDate == null) {
					return "";
				}
				return dateTimeFormatter.format(localDate);
			}

			@Override
			public LocalDate fromString(String dateString) {
				if (dateString == null || dateString.trim().isEmpty()) {
					return null;
				}
				return LocalDate.parse(dateString, dateTimeFormatter);
			}
		};
	}

	static void createSettingsStage() {
		settingsStage = new Stage();

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10));
		vbox.setSpacing(8);

		Text sheetIdLabel = new Text("Sheet ID");
		sheetIdLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		vbox.getChildren().add(sheetIdLabel);

		sheetId = new TextField(CerealTables.sheetId);
		sheetId.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		vbox.getChildren().add(sheetId);

		Text sheetNameLabel = new Text("Sheet Name");
		sheetNameLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		vbox.getChildren().add(sheetNameLabel);

		sheetName = new TextField(CerealTables.sheetName);
		sheetName.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		Text fromRowToRowLabel = new Text("From Row : To Row");
		fromRowToRowLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		fromRow = new TextField(CerealTables.fromRow);
		fromRow.setPromptText("From Row");
		fromRow.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		Text semiColon = new Text(" : ");
		semiColon.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		toRow = new TextField(CerealTables.toRow);
		toRow.setPromptText("To Row");
		toRow.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		HBox cellshbox = new HBox();
		cellshbox.setPadding(new Insets(0, 0, 0, 0));
		cellshbox.setSpacing(5);
		cellshbox.getChildren().add(fromRow);
		cellshbox.getChildren().add(semiColon);
		cellshbox.getChildren().add(toRow);

		sheetName = new TextField(CerealTables.sheetName);
		sheetName.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		vbox.getChildren().add(sheetName);
		vbox.getChildren().add(fromRowToRowLabel);
		vbox.getChildren().add(cellshbox);

		buttonSave = new Button("Save");
		buttonSave.setPrefSize(100, 20);
		vbox.getChildren().add(buttonSave);

		Button buttonClose = new Button("Close");
		buttonClose.setPrefSize(100, 20);
		vbox.getChildren().add(buttonClose);
		buttonClose.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				settingsStage.hide();
			}
		});

		Scene settingsScene = new Scene(vbox, 500, 400);

		settingsStage.setScene(settingsScene);
		settingsStage.setTitle("Settings");
		settingsStage.initModality(Modality.WINDOW_MODAL);
	}

	public static void appendLog(final String string) {
    Calendar cal = Calendar.getInstance();
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		final String append = sdf.format(cal.getTime()) + " " + string + "\n";
		if (Platform.isFxApplicationThread()) {
			logArea.setText(append + logArea.getText());
		} else {
			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					logArea.setText(append + logArea.getText());
				}
			});
		}
	}

	public static VBox addImageBox() {
		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10));
		vbox.setSpacing(8);

		imageTitle = new Text("Images");
		imageTitle.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		vbox.getChildren().add(imageTitle);

		imageView = new ImageView();
		imageView.setFitHeight(350);
		imageView.setFitWidth(350);
		imageView.setPreserveRatio(true);

		vbox.getChildren().add(imageView);

		return vbox;
	}

	public static VBox addLeftBox() {
		VBox logvbox = new VBox();
		logvbox.setPadding(new Insets(10));
		logvbox.setSpacing(8);

		Text title = new Text("Logs");
		title.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		logArea = new TextArea();
		logArea.setPrefWidth(400);
		logArea.setEditable(false);
		logArea.setFont(Font.font("Arial", FontWeight.EXTRA_LIGHT, 10));

		logvbox.getChildren().add(title);
		logvbox.getChildren().add(logArea);

		configBox = new VBox();
		configBox.setPadding(new Insets(10));
		configBox.setSpacing(8);
		configBox.getChildren().add(getMainConfigPane());
		configBox.getChildren().add(getWheatCountingPane());
		configBox.getChildren().add(getProcessAllCheckbox());
		configBox.getChildren().add(getContinuouslyProcessCheckbox());
		configBox.getChildren().add(getFilterBox());

		logvbox.getChildren().add(configBox);
		return logvbox;
	}

	private static Node getProcessAllCheckbox() {
		processAllCheckbox = new CheckBox("Process all records in table");
		processAllCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		processAllCheckbox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				filtervbox.setVisible(!processAllCheckbox.isSelected());
				processContinuouslyCheckbox.setSelected(false);
				processUnprocessedCheckbox.setDisable(false);
			}
		});
		return processAllCheckbox;
	}

	private static Node getContinuouslyProcessCheckbox() {
		processContinuouslyCheckbox = new CheckBox("Continuously process records");
		processContinuouslyCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		processContinuouslyCheckbox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				if (processContinuouslyCheckbox.isSelected()) {
					processAllCheckbox.setSelected(false);
					filtervbox.setVisible(true);
					processUnprocessedCheckbox.setSelected(true);
					processUnprocessedCheckbox.setDisable(true);
				} else {
					processUnprocessedCheckbox.setDisable(false);
				}
			}
		});
		return processContinuouslyCheckbox;
	}

	private static Node getFilterBox() {
		Text columnNameFilterLabel = new Text("Filter by");
		columnNameFilterLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		filtervbox = new VBox();
		filtervbox.setPadding(new Insets(10));
		filtervbox.setSpacing(8);
		filtervbox.getChildren().add(columnNameFilterLabel);

		processUnprocessedCheckbox = new CheckBox("Only unprocessed images");
		processUnprocessedCheckbox.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		processUnprocessedCheckbox.setSelected(true);

		HBox columnhbox = new HBox();
		columnhbox.setPadding(new Insets(5, 5, 5, 5));
		columnhbox.setSpacing(5);

		columnNameFilterKey = new TextField("");
		columnNameFilterKey.setPromptText("Column Name");
		columnNameFilterKey.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		columnNameFilterValue = new TextField("");
		columnNameFilterValue.setPromptText("Column Value");
		columnNameFilterValue.setFont(Font.font("Arial", FontWeight.BOLD, 14));

		columnhbox.getChildren().add(columnNameFilterKey);
		columnhbox.getChildren().add(columnNameFilterValue);

		HBox datehbox = new HBox();
		datehbox.setPadding(new Insets(5, 5, 5, 5));
		datehbox.setSpacing(5);

		fromDatePicker = new DatePicker();
		fromDatePicker.setConverter(dateConverter);
		toDatePicker = new DatePicker();
		toDatePicker.setConverter(dateConverter);
		datehbox.getChildren().add(fromDatePicker);
		datehbox.getChildren().add(toDatePicker);

		filtervbox.getChildren().add(processUnprocessedCheckbox);
		filtervbox.getChildren().add(columnhbox);
		filtervbox.getChildren().add(datehbox);
		return filtervbox;
	}

	private static Node getMainConfigPane() {
		GridPane mainConfigPane = new GridPane();
		mainConfigPane.setHgap(10);
		mainConfigPane.setVgap(10);
		ColumnConstraints labelColumn = new ColumnConstraints(100, 100, 100);
		ColumnConstraints fieldColumn = new ColumnConstraints(160, 160, Double.MAX_VALUE);
		fieldColumn.setHgrow(Priority.ALWAYS);
		mainConfigPane.getColumnConstraints().addAll(labelColumn, fieldColumn);

		return mainConfigPane;
	}

	private static Node getWheatCountingPane() {
		GridPane wheatCountingPane = new GridPane();
		wheatCountingPane.setHgap(10);
		wheatCountingPane.setVgap(10);
		ColumnConstraints labelColumn = new ColumnConstraints(100, 100, 100);
		ColumnConstraints fieldColumn = new ColumnConstraints(160, 160, Double.MAX_VALUE);
		wheatCountingPane.getColumnConstraints().addAll(labelColumn, fieldColumn);

		Label focalLengthLabel = new Label("Focal Length");
		focalLengthLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		focalLength = new TextField(String.valueOf(CerealTables.focalLength));
		focalLength.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		wheatCountingPane.add(focalLengthLabel, 0, 1);
		wheatCountingPane.add(focalLength, 1, 1);

		Label hDistanceLabel = new Label("H Distance");
		hDistanceLabel.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		hDistance = new TextField(String.valueOf(CerealTables.hDistance));
		hDistance.setFont(Font.font("Arial", FontWeight.BOLD, 14));
		wheatCountingPane.add(hDistanceLabel, 0, 2);
		wheatCountingPane.add(hDistance, 1, 2);
		wecGridPane = wheatCountingPane;
		wecGridPane.managedProperty().bind(wecGridPane.visibleProperty());
		return wheatCountingPane;
	}

	public static VBox addTopBox() {
		algoBox = new HBox();
		algoBox.setPadding(new Insets(5, 0, 5, 0));
		algoBox.setSpacing(10);

		earlyVigorAlgo = new CheckBox("Early Vigor");
		earlyVigorAlgo.setPadding(new Insets(0, 17, 0, 0));

		earCountingAlgo = new CheckBox("Wheat Ear Counting");
		earCountingAlgo.setPadding(new Insets(0, 17, 0, 0));

		maturityAlgo = new CheckBox("Maturity");
		maturityAlgo.setPadding(new Insets(0, 17, 0, 0));

		algoBox.getChildren().addAll(earlyVigorAlgo, earCountingAlgo, maturityAlgo);

		HBox controlBox = addControlBox();

		VBox vbox = new VBox();
		vbox.setPadding(new Insets(10));
		vbox.setSpacing(8);
		vbox.getChildren().add(algoBox);
		vbox.getChildren().add(controlBox);
		vbox.setStyle("-fx-background-color: #dddddd;");

		return vbox;
	}

	public static HBox addControlBox() {
		HBox hbox = new HBox();
		hbox.setSpacing(10);

		buttonStart = new Button("Start");
		buttonStart.setPrefSize(100, 20);

		buttonStop = new Button("Stop");
		buttonStop.setPrefSize(100, 20);

		Button buttonClear = new Button("Clear logs");
		buttonClear.setPrefSize(100, 20);
		buttonClear.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				logArea.setText("");
			}
		});

		buttonSettings = new Button("Settings");
		buttonSettings.setPrefSize(100, 20);
		buttonSettings.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				openSettings(event);
			}
		});

		Button buttonRefreshToken = new Button("Re-Auth");
		buttonRefreshToken.setPrefSize(100, 20);
		buttonRefreshToken.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String[] entries = CerealTables.TOKENS_DIRECTORY_PATH.list();
				for (String s : entries) {
					File currentFile = new File(CerealTables.TOKENS_DIRECTORY_PATH.getPath(), s);
					currentFile.delete();
				}
				CerealTables.initFusionAPI();
			}
		});

		hbox.getChildren().addAll(buttonSettings, buttonStart, buttonStop, buttonClear, buttonRefreshToken);

		return hbox;
	}

	public static void disableUI(boolean disable) {
		buttonStart.setDisable(disable);
		algoBox.setDisable(disable);
		configBox.setDisable(disable);
	}

	static void openSettings(ActionEvent event) {
		sheetId.setText(CerealTables.sheetId);
		sheetName.setText(CerealTables.sheetName);
		fromRow.setText(CerealTables.fromRow);
		toRow.setText(CerealTables.toRow);
		if (settingsStage.getOwner() == null) {
			settingsStage.initOwner(((Node) event.getSource()).getScene().getWindow());
		}
		settingsStage.show();
	}
}
