package com.utils;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Alexi Akl (alexiakl@gmail.com)
 *
 */
public class CSVUtils {

	public static File csvFile;

	public static boolean createCSVFile() {
		String delim = ",";
		StringBuilder csvBuilder = new StringBuilder();
		csvBuilder.append("Record ID");
		csvBuilder.append(delim);
		csvBuilder.append("Early Vigor");
		csvBuilder.append(delim);
		csvBuilder.append("Ear Count");
		csvBuilder.append(delim);
		csvBuilder.append("Maturity");
		csvBuilder.append(delim);
		csvBuilder.append(System.getProperty("line.separator"));

		String csvFilename = new SimpleDateFormat("yyyyMMddHHmmss'.csv'").format(new Date());
		File csvDirectory = new File("csv");
		csvFile = new File(csvDirectory.getAbsolutePath() + File.separator + csvFilename);
		try {
			if (!csvDirectory.exists()) {
				if (!csvDirectory.mkdir()) {
					return false;
				}
			}
			if (!csvFile.createNewFile()) {
				return false;
			}
			Files.write(Paths.get(csvFile.getAbsolutePath()), csvBuilder.toString().getBytes(), StandardOpenOption.APPEND);
			UIUtils.appendLog("CSV file created: " + csvFile.getAbsolutePath());
		} catch (IOException e) {
			UIUtils.appendLog("Failed to create CSV file, aborting");
			return false;
		}

		return true;
	}

	public static void appendResultToCSV(Double earlyVigor, Integer earCount, Double maturity, String recordId) {
		String delim = ",";
		StringBuilder csvBuilder = new StringBuilder();

		csvBuilder.append(recordId);
		csvBuilder.append(delim);
		if (earlyVigor != null) {
			csvBuilder.append(earlyVigor);
		}
		csvBuilder.append(delim);
		if (earCount != null) {
			csvBuilder.append(earCount);
		}
		csvBuilder.append(delim);
		if (maturity != null) {
			csvBuilder.append(maturity);
		}
		csvBuilder.append(System.getProperty("line.separator"));

		try {
			Files.write(Paths.get(csvFile.getAbsolutePath()), csvBuilder.toString().getBytes(), StandardOpenOption.APPEND);
			UIUtils.appendLog("Result appended to CSV file");
		} catch (IOException e) {
			UIUtils.appendLog("Failed to append result to CSV file");
		}
	}
}
