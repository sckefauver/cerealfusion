package com;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;

import com.algorithms.breedpix.BreedPixResult;
import com.algorithms.breedpix.BreedPixTool;
import com.algorithms.breedpix.PicVIOperation;
import com.algorithms.earlyvigor.EarlyVigorAreaEstimator;
import com.algorithms.wec.WheatEarCounter;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.utils.CSVUtils;
import com.utils.UIUtils;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author Alexi Akl (alexiakl@gmail.com)
 *
 */
@SuppressWarnings("restriction")
public class CerealTables extends Application {

	private static final List<String> SCOPES = Arrays.asList(SheetsScopes.SPREADSHEETS);
	public static final File TOKENS_DIRECTORY_PATH = new java.io.File(System.getProperty("user.home"),
	    ".store/sheet_tables");
	private static final String APPLICATION_NAME = "Cereal-FusionTables/1.0";
	public static String sheetId;
	public static String sheetName;
	public static String fromRow;
	public static String toRow;
	public static double focalLength;
	public static double hDistance;
	public static HashMap<String, Integer> columnsMap = new HashMap<String, Integer>();

	static boolean processShouldBeRunning = false;
	static Thread processThread;
	static Timer processTimer;
	private Preferences prefs;

	private static final double H_DISTANCE = 0.8;
	private static final double FOCAL_LENGTH = 14;

	private static Sheets service;

	private static NetHttpTransport HTTP_TRANSPORT;

	/** Global instance of the JSON factory. */
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	public static void main(String[] args) {
		launch(args);
	}

	@SuppressWarnings("unused")
	private static void testLocalImage() {
		File imageFile = new File("/Users/alexiakl/Downloads/P6086305.JPG");
		BufferedImage bf;
		try {
			bf = ImageIO.read(imageFile);
			int wheatEarCounter = WheatEarCounter.count(bf, H_DISTANCE, FOCAL_LENGTH);
			System.out.println(wheatEarCounter);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		prefs = Preferences.userRoot().node(this.getClass().getName());
		sheetId = prefs.get("sheetId", "1MZodidMjIJliw0ta_ExKWF20MtoET6CL30jg0TTpEAU");
		sheetName = prefs.get("sheetName", "CerealsFusion");
		fromRow = prefs.get("fromRow", "2");
		toRow = prefs.get("toRow", "20");
		focalLength = prefs.getDouble("focalLength", FOCAL_LENGTH);
		hDistance = prefs.getDouble("hDistance", H_DISTANCE);
		Scene scene = UIUtils.buildUI();
		primaryStage.setTitle("Cereal Tables");
		primaryStage.setScene(scene);
		primaryStage.setMinHeight(scene.getHeight());
		primaryStage.setMinWidth(scene.getWidth());
		primaryStage.show();

		HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
		initFusionAPI();
		initApplication();
	}

	private void initApplication() {
		UIUtils.buttonSave.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				savePressed();
			}
		});

		UIUtils.buttonStart.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				startPressed();
			}
		});

		UIUtils.buttonStop.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				stopPressed();
			}
		});
	}

	protected void savePressed() {
		String tempSheetId = UIUtils.sheetId.getText();
		if (tempSheetId.compareTo(sheetId) != 0) {
			sheetId = tempSheetId;
			UIUtils.appendLog("Storing sheet ID");
			prefs.put("sheetId", tempSheetId);
		}

		String tempSheetName = UIUtils.sheetName.getText();
		if (tempSheetName.compareTo(sheetName) != 0) {
			sheetName = tempSheetName;
			UIUtils.appendLog("Storing sheet name");
			prefs.put("sheetName", tempSheetName);
		}

		String tempFromRow = UIUtils.fromRow.getText();
		if (tempFromRow.compareTo(fromRow) != 0) {
			fromRow = tempFromRow;
			UIUtils.appendLog("Storing from row");
			prefs.put("fromRow", tempFromRow);
		}

		String tempToRow = UIUtils.toRow.getText();
		if (tempToRow.compareTo(toRow) != 0) {
			toRow = tempToRow;
			UIUtils.appendLog("Storing to row");
			prefs.put("toRow", tempToRow);
		}

		prefs.putDouble("focalLength", Double.parseDouble(UIUtils.focalLength.getText()));
		prefs.putDouble("hDistance", Double.parseDouble(UIUtils.hDistance.getText()));

		try {
			prefs.sync();
			UIUtils.settingsStage.hide();
		} catch (BackingStoreException exception) {
			exception.printStackTrace();
		}
	}

	static void startPressed() {
		if (UIUtils.earCountingAlgo.isSelected()) {
			try {
				Double.parseDouble(UIUtils.focalLength.getText());
				Double.parseDouble(UIUtils.hDistance.getText());
			} catch (Exception e) {
				UIUtils.appendLog("Please provide a double value for H Distance and Focal Length");
				return;
			}
		}
		if (!UIUtils.earCountingAlgo.isSelected() && !UIUtils.earlyVigorAlgo.isSelected()
		    && !UIUtils.maturityAlgo.isSelected()) {
			UIUtils.appendLog("Please select at least one algo");
			return;
		}

		String finalFromRow = fromRow;
		String finalToRow = toRow;
		if (UIUtils.processContinuouslyCheckbox.isSelected()) {
			finalFromRow = "2";
			finalToRow = "30000";
		}
		UIUtils.disableUI(true);
		UIUtils.appendLog("Starting process");
		UIUtils.appendLog("Sheet ID: " + sheetId);
		UIUtils.appendLog("Sheet Name: " + sheetName);
		UIUtils.appendLog("Rows: [" + finalFromRow + ", " + finalToRow + "]");

		processThread = new Thread(new Runnable() {
			@Override
			public void run() {
				processRows();
			}
		});
		processThread.start();
	}

	void stopPressed() {
		if (processTimer != null) {
			processTimer.cancel();
			processTimer = null;
			UIUtils.appendLog("Timer canceled");
			processShouldBeRunning = false;
			UIUtils.disableUI(false);
		}

		if (!processShouldBeRunning) {
			UIUtils.appendLog("Process is not running");
		} else {
			UIUtils.appendLog("Process will halt once download / process of current image is complete");
			UIUtils.appendLog("Please wait");
		}
		processShouldBeRunning = false;
	}

	public static void initFusionAPI() {
		try {
			service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
			    .setApplicationName(APPLICATION_NAME).build();
		} catch (Exception e) {
			UIUtils.appendLog("Sheets API failed to initialize: " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		InputStream in = CerealTables.class.getResourceAsStream("/client_secrets.json");
		if (in == null) {
			in = ClassLoader.getSystemClassLoader().getResourceAsStream("resources/client_secrets.json");
		}
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
		    clientSecrets, SCOPES).setDataStoreFactory(new FileDataStoreFactory(TOKENS_DIRECTORY_PATH))
		        .setAccessType("offline").build();
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receiver).authorize("user");
	}

	static void processRows() {
		try {
			String finalFromRow = fromRow;
			String finalToRow = toRow;
			if (UIUtils.processContinuouslyCheckbox.isSelected()) {
				finalFromRow = "2";
				finalToRow = "30000";
			}

			updateColumnNames();

			processShouldBeRunning = true;
			PicVIOperation picViOperation = new PicVIOperation();

			columnsMap.clear();
			int imageColumnIndex = -1;
			int instanceIdIndex = -1;
			int wecIndex = -1;
			int evIndex = -1;
			int csiIndex = -1;

			ValueRange columnsResponse = service.spreadsheets().values().get(sheetId, "!1:1").execute();
			List<List<Object>> columnValues = columnsResponse.getValues();
			if (columnValues == null || columnValues.isEmpty()) {
				UIUtils.appendLog("No columns found.");
				return;
			}
			for (List<?> columns : columnValues) {
				for (int i = 0; i < columns.size(); i++) {
					String columnName = columns.get(i).toString().toLowerCase();
					columnsMap.put(columnName, Integer.valueOf(i));
					if (columnName.startsWith("image2")) {
						imageColumnIndex = i;
					} else if (columnName.startsWith("instanceid")) {
						instanceIdIndex = i;
					} else if (columnName.startsWith("earlyvigor")) {
						evIndex = i;
					} else if (columnName.startsWith("earcount")) {
						wecIndex = i;
					} else if (columnName.startsWith("maturity")) {
						csiIndex = i;
					}
				}
			}

			if (imageColumnIndex < 0 || instanceIdIndex < 0) {
				UIUtils.appendLog("No Image or InstanceID columns found.");
				return;
			}

			StringBuilder range = new StringBuilder("!");
			range.append(finalFromRow).append(":").append(finalToRow);
			ValueRange response = service.spreadsheets().values().get(sheetId, range.toString()).execute();

			int processed = 0;
			boolean processAll = UIUtils.processAllCheckbox.isSelected();
			boolean processUnprocessed = false;
			Integer filterIndex = null, dateIndex = null;
			LocalDate filterFromDate = null, filterToDate = null;
			if (!processAll) {
				processUnprocessed = UIUtils.processUnprocessedCheckbox.isSelected();
				String filterKey = UIUtils.columnNameFilterKey.getText();
				filterIndex = columnsMap.get(filterKey);
				dateIndex = columnsMap.get("metasubmissiondate");
				filterFromDate = UIUtils.fromDatePicker.getValue();
				filterToDate = UIUtils.toDatePicker.getValue();
			}
			List<List<Object>> values = response.getValues();

			if (values == null || values.isEmpty()) {
				UIUtils.appendLog("No data found.");
			} else {
				boolean createFileAttempt = false;
				int i = 0;
				UIUtils.appendLog("Available records: " + values.size());
				for (List<?> row : values) {
					if (row.size() <= imageColumnIndex) {
						i++;
						continue;
					}
					String reason = "";
					String imageString = row.get(imageColumnIndex).toString();
					String instanceID = row.get(instanceIdIndex).toString();
					String earlyVigorString = "";
					if (evIndex > 0 && row.size() > evIndex) {
						earlyVigorString = row.get(evIndex).toString();
					}
					String wecString = "";
					if (wecIndex > 0 && row.size() > wecIndex) {
						wecString = row.get(wecIndex).toString();
					}
					String csiString = "";
					if (csiIndex > 0 && row.size() > csiIndex) {
						csiString = row.get(csiIndex).toString();
					}

					boolean validImage = imageString.startsWith("http");
					boolean process = false;
					if (validImage) {
						process = processAll;
						if (!process) {
							boolean skip = false;
							if (processUnprocessed) {
								process = processUnprocessed && UIUtils.earCountingAlgo.isSelected() && wecString.length() == 0;
								if (!process) {
									process = processUnprocessed && UIUtils.earlyVigorAlgo.isSelected() && earlyVigorString.length() == 0;
								}
								if (!process) {
									process = processUnprocessed && UIUtils.maturityAlgo.isSelected() && csiString.length() == 0;
								}
								if (!process) {
									skip = true;
									reason = "already processed";
								}
							}
							if (!skip && filterIndex != null && filterIndex > -1 && row.size() > filterIndex) {
								String filterValue = row.get(filterIndex).toString();
								if (UIUtils.columnNameFilterValue.getText().compareToIgnoreCase(filterValue) != 0) {
									process = false;
									skip = true;
									reason = "filter doesn't match";
								} else {
									process = true;
								}
							}
							if (!skip && dateIndex != null && dateIndex > -1 && filterFromDate != null && filterToDate != null
							    && row.size() > dateIndex) {
								String dateValue = row.get(dateIndex).toString();
								try {
									dateValue = dateValue.split(" ")[0];
									DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
									LocalDate date = LocalDate.parse(dateValue, formatter);
									process = (date.isEqual(filterFromDate) || date.isAfter(filterFromDate))
									    && (date.isEqual(filterToDate) || date.isBefore(filterToDate));
									if (!process) {
										reason = "not in date interval";
									}
								} catch (Exception e) {
								}
							}
						}
					} else {
						reason = "image URL not valid: " + imageString;
					}

					i++;
					if (process) {
						URL imageurl = new URL(imageString);
						if (shouldHalt()) {
							return;
						}
						final int index = i;
						UIUtils.appendLog("Downloading image " + index + " of " + values.size());
						final BufferedImage image = ImageIO.read(imageurl);
						if (shouldHalt()) {
							return;
						}

						final BufferedImage scaledRenderedImage = BreedPixTool.reduceToMaxSize(image, 1024 * 768);
						Platform.runLater(new Runnable() {
							@Override
							public void run() {
								UIUtils.appendLog("Processing Image");
								Image transimage = SwingFXUtils.toFXImage(scaledRenderedImage, null);
								UIUtils.imageView.setImage(transimage);
								UIUtils.imageTitle.setText("Image " + index);
							}
						});

						Double csi = null;
						Integer wheatEarCounter = null;
						Double earlyVigorArea = null;
						if (UIUtils.maturityAlgo.isSelected()) {
							BreedPixResult result = picViOperation.execute(scaledRenderedImage);
							csi = Double.valueOf(result.getCsi());
							UIUtils.appendLog("Maturity: " + result.getCsi());
						}
						if (UIUtils.earCountingAlgo.isSelected()) {
							double hDistance = Double.parseDouble(UIUtils.hDistance.getText());
							double focalLength = Double.parseDouble(UIUtils.focalLength.getText());
							wheatEarCounter = Integer.valueOf(WheatEarCounter.count(image, hDistance, focalLength));
							UIUtils.appendLog("WheatEarCounter: " + wheatEarCounter);
						}
						if (UIUtils.earlyVigorAlgo.isSelected()) {
							earlyVigorArea = Double.valueOf(EarlyVigorAreaEstimator.estimateArea(image));
							UIUtils.appendLog("EarlyVigorArea: " + earlyVigorArea);
						}

						if (!createFileAttempt) {
							createFileAttempt = true;
							if (!CSVUtils.createCSVFile()) {
								UIUtils.appendLog("Failed to create CSV file");
							}
						}
						
						processed++;
						CSVUtils.appendResultToCSV(earlyVigorArea, wheatEarCounter, csi, instanceID);
						executeUpdate(earlyVigorArea, wheatEarCounter, csi, Integer.parseInt(fromRow) + i - 1);
					} else {
						if (!UIUtils.processContinuouslyCheckbox.isSelected()) {
							UIUtils.appendLog("Skipping row " + i + " of " + values.size() + "; Reason: " + reason);
						}
					}
				}
			}
			
			UIUtils.appendLog("Records processed: " + processed);
			if (UIUtils.processContinuouslyCheckbox.isSelected()) {
				UIUtils.appendLog("Checking for new records in 60 seconds...");
				processTimer = new Timer();
				processTimer.schedule(new java.util.TimerTask() {
					@Override
					public void run() {
						processTimer.cancel();
						processTimer = null;
						processRows();
					}
				}, 60000);
			} else {
				processShouldBeRunning = false;
				UIUtils.disableUI(false);
				new Timer().schedule(new java.util.TimerTask() {
					@Override
					public void run() {
						UIUtils.appendLog("Done");
					}
				}, 500);
			}
		} catch (Exception e) {
			UIUtils.appendLog("Error while processing image: " + e.getMessage());
			UIUtils.appendLog("Process Halted");
			UIUtils.disableUI(false);
			e.printStackTrace();
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void updateColumnNames() {
		ValueRange body = new ValueRange();
		List values = Arrays.asList(Arrays.asList("EarlyVigor", "EarCount", "Maturity"));
		body.setValues(values);
		try {
			service.spreadsheets().values().update(sheetId, "AH1", body).setValueInputOption("RAW").execute();
		} catch (IOException exception) {
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void executeUpdate(Double earlyVigor, Integer wheatEarCounter, Double csi, Integer index) {
		ValueRange body = new ValueRange();
		ArrayList<Object> list = new ArrayList<Object>();
		if (earlyVigor != null) {
			list.add(earlyVigor);
		} else {
			list.add("");
		}
		if (wheatEarCounter != null) {
			list.add(wheatEarCounter);
		} else {
			list.add("");
		}
		if (csi != null) {
			list.add(csi);
		} else {
			list.add("");
		}
		List values = Arrays.asList(list);
		body.setValues(values);
		try {
			service.spreadsheets().values().update(sheetId, "AH" + index, body).setValueInputOption("RAW").execute();
			UIUtils.appendLog("Result stored in sheet");
		} catch (IOException exception) {
			UIUtils.appendLog("Failed to store result in sheet");
		}
	}

	private static boolean shouldHalt() {
		if (!processShouldBeRunning) {
			UIUtils.appendLog("Process Halted");
			UIUtils.disableUI(false);
			return true;
		}
		return false;
	}
}
