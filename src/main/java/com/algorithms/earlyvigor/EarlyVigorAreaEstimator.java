package com.algorithms.earlyvigor;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.image.BufferedImage;
import com.plugins.Color_Modified2;
import com.plugins.StackEditor;
import ij.ImagePlus;
import ij.measure.Measurements;
import ij.measure.ResultsTable;
import ij.plugin.filter.Analyzer;

/**
 * 
 * @author J.A. Fernandez-Gallego
 * <p>
 * Original macro algorithm
 * <pre>
 *      run("Color Modified2", "colour=LCHLuv2");   // Obtained from Color_Modified2.ijm 
 *      run("Stack to Images");                     // H Matrix save angle in degrees
 *      selectWindow("C");                          // C Matrix save 'Binany' output
 *      run("Set Measurements...", "area area_fraction redirect=None decimal=3");
 *      run("Measure");
 *      var Area = getResult("%Area", 0);
 * </pre>
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Conversion into Java code
 *
 */
public final class EarlyVigorAreaEstimator {

        private EarlyVigorAreaEstimator() {
                
        }
        
        public static final String estimateArea(BufferedImage image) throws NullPointerException {
                if(image == null) {
                        throw new NullPointerException("image cannot be null");
                }
                
                String area = "-1";
                
                ImagePlus imp = new ImagePlus("", image);
                Color_Modified2 cm2 = new Color_Modified2();
                ImagePlus imp2 = cm2.run(imp.getChannelProcessor(), "LCHLuv2");
                
                int measurements = Measurements.AREA + Measurements.AREA_FRACTION;
                Analyzer.setPrecision(3);
                Analyzer.setMeasurements(measurements);
                
                StackEditor se = new StackEditor();
                ImagePlus[] imgStacks = se.convertStackToImages(imp2);
                for(ImagePlus stack : imgStacks) {
                        if(stack.getTitle().equals("C")) {
                                ResultsTable rt = new ResultsTable();
                                Analyzer analyzer = new Analyzer(stack, rt);
                                analyzer.measure();
                                area = rt.getStringValue("%Area", 0);
                                break;
                        }
                }
                
                return area;
        }
}
