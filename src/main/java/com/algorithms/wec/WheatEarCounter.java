package com.algorithms.wec;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Polygon;
import java.awt.image.BufferedImage;
import com.plugins.Laplacian;
import ij.ImagePlus;
import ij.plugin.ChannelSplitter;
import ij.plugin.filter.MaximumFinder;
import ij.process.ImageProcessor;
import net.sf.ij_plugins.filters.FastMedian;

/**
 * 
 * @author J.A. Fernandez-Gallego
 * <p>
 * Original macro algorithm
 * <pre>
 *  run("Duplicate...", "title=Imag");
 *  run("Laplacian");
 *  run("Fast Median ...", "filter=64");
 *  run("Find Maxima...", "noise=20 output=List");
 *  refArea = nResults;
 * </pre>
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Conversion into Java code
 *
 */

public final class WheatEarCounter {

        private static final double H_DISTANCE = 0.8;   // User entered
        private static final double FOCAL_LENGTH = 14;  // User entered
        
        private static final double SENSOR_WIDTH = 17.3;
        private static final double IMAGE_WIDTH = 4608;
        private static final double CONSTANT_WIDTH = SENSOR_WIDTH / IMAGE_WIDTH;
    
        private WheatEarCounter() {
                
        }

        public static final int count(BufferedImage image, double h_distance, double focal_length) throws NullPointerException {
                // Assign defaults to values if the user
                // did not enter them. They should though.
                if(h_distance < 0) {
                    h_distance = H_DISTANCE;
                }
                
                if(focal_length < 0) {
                    focal_length = FOCAL_LENGTH;
                }
                
                double GSD = ((CONSTANT_WIDTH * h_distance) / (focal_length)) * 100;
                int varMedian = (int)Math.round(1.37/GSD);
                
                int count = -1;
                
                ImagePlus imp = new ImagePlus("", image);
                
                Laplacian lap = new Laplacian();
                lap.setup(imp);
                ImagePlus impFFTInverse = lap.filtering();
                if(impFFTInverse != null) {
                        ImageProcessor ip = FastMedian.process(impFFTInverse.getChannelProcessor(), varMedian);
                        MaximumFinder mf = new MaximumFinder();
                        Polygon poly = mf.getMaxima(ip, 25, false);
                        count = poly.npoints;
                }
                else {
                        count = -2;
                }
                
                return count;
        }
        
        @SuppressWarnings("unused")
        private static ImagePlus getBlueChannel(ImagePlus impFFTInverse) {
                ImagePlus[] channels = ChannelSplitter.split(impFFTInverse);
                ImagePlus impBlueChannel = null;
                for(ImagePlus impChannel : channels) {
                        if("blue".equals(impChannel.getTitle())) {
                                impBlueChannel = impChannel;
                                break;
                        }
                }
                
                return impBlueChannel;
        }
}

