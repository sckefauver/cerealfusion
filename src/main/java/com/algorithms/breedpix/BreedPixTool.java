package com.algorithms.breedpix;

/*
 * Cereal Fusion Tables
 * 
 * Copyright (C) 2018  Shawn Carlisle Kefauver
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;

/**
 * 
 * @author George El Haddad (george.dma@gmail.com)
 * <p>
 * Created on: Nov 29, 2017
 */
public final class BreedPixTool {

    private BreedPixTool() {
            
    }
    
    /**
     * Copyright 2012 Jaume Casadesus
     * <p>
     * This code block can not be copied and/or distributed without the express
     * permission of Jaume Casadesus
     * 
     * @author Jaume Casadesus (jaume.casadesus@irta.cat)
     */
    public static final BufferedImage reduceToMaxSize(BufferedImage image, int maxPixels) {
            int size = image.getHeight() * image.getWidth();

            double scale = 1.0D;
            BufferedImage rimg;
            if (size > maxPixels) {
                    scale = Math.sqrt(maxPixels * 1.0D / size);
                    rimg = scaleImage(image, scale);
            }
            else {
                    rimg = image;
            }
            return rimg;
    }
    
    /**
     * Copyright 2012 Jaume Casadesus
     * <p>
     * This code block can not be copied and/or distributed without the express
     * permission of Jaume Casadesus
     * 
     * @author Jaume Casadesus (jaume.casadesus@irta.cat)
     */
    public static final BufferedImage scaleImage(BufferedImage image, double scale) {
            return scaleImage(image, (int) (image.getWidth() * scale), (int) (image.getHeight() * scale));
    }
    
    /**
     * Copyright 2012 Jaume Casadesus
     * <p>
     * This code block can not be copied and/or distributed without the express
     * permission of Jaume Casadesus
     * 
     * @author Jaume Casadesus (jaume.casadesus@irta.cat)
     */
    public static final BufferedImage scaleImage(BufferedImage image, int targetWidth, int targetHeight) {
            int imgType = image.getType();

            BufferedImage newImg = new BufferedImage(targetWidth, targetHeight, imgType);
            Graphics2D g2 = newImg.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g2.drawImage(image, 0, 0, targetWidth, targetHeight, 0, 0, image.getWidth(), image.getHeight(), null);
            return newImg;
    }
    
    /**
     * Copyright 2012 Jaume Casadesus
     * <p>
     * This code block can not be copied and/or distributed without the express
     * permission of Jaume Casadesus
     * 
     * @author Jaume Casadesus (jaume.casadesus@irta.cat)
     */
    public static final BufferedImage paintBWNotROI(RenderedImage img, PixelMask roi, String imageName) {
            BufferedImage bi = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
            try {
                    // Create the scale operation

                    int numXTiles = img.getNumXTiles();
                    int numYTiles = img.getNumYTiles();
                    WritableRaster wrRaster = null;
                    // sets the value for each pixel
                    int[] pixel = new int[3];

                    for (int i = 0; i < numXTiles; i++) {
                            for (int j = 0; j < numYTiles; j++) {
                                    wrRaster = img.getTile(i, j).createCompatibleWritableRaster();
                                    wrRaster = img.copyData(wrRaster);
                                    int minX = wrRaster.getMinX();
                                    int maxX = minX + wrRaster.getWidth();
                                    int minY = wrRaster.getMinY();
                                    int maxY = minY + wrRaster.getHeight();

                                    for (int x = minX; x < maxX; x++) {
                                            for (int y = minY; y < maxY; y++) {
                                                    if (!roi.contains(x, y)) {
                                                            pixel = wrRaster.getPixel(x, y, pixel);
                                                            pixel[0] = (pixel[0] + pixel[1] + pixel[2]) / 6;
                                                            pixel[1] = pixel[0];
                                                            pixel[2] = pixel[0];
                                                            wrRaster.setPixel(x, y, pixel);
                                                    }
                                            }
                                    }
                                    bi.setData(wrRaster);
                            }
                    }
            }
            catch (Exception ex) {
                    //IJ.log("Error rendering ROI for image: " + imageName);
                    //IJ.log("Error: "+ex.getMessage());
            }
            
            return bi;
    }
}
